# tutorial
https://docs.microsoft.com/ja-jp/azure/aks/kubernetes-walkthrough

# ゴール

- kubernetesの概要を理解
- AKS(Azure Kubernetes Service)を使ってk8s環境を構築する

## なにはともあれ(AKSで)k8sクラスタを作ってみましょう

- Azureにログインし、上部検索ウインドウに「kubernetes」と入力する。
- kubernetes(サービス)を選択する。

![作成画面](img/kubernetes-service.png)
kubernetesサービスの作成ボタンから、クラスタ作成画面へ移行します。

![作成画面](img/create_cluster.png)
- DNS名のプレフィックス
  - 今回はAPIは利用せず`kubectl`コマンドでクラスタ操作を行うので適当でOKです。
- ノードサイズ、ノード数
  - VMの指定になります。デフォルトの3のまま作ります。
  - AKSではVMのスペックは後から変更できないので注意が必要です。

作成に20分弱かかるので、以降少し座学をやります。

## はじめに

kubernetesはdockerコンテナ周りのデプロイ、運用などを行う製品です。
俗にいうオーケストレーションツール。

例えばですが、
- コンテナにどれだけのCPU/MEMのリソースを割り当てるか
- コンテナのヘルスチェック
- オートヒーリング
- オートスケーリング
- コンテナ作成時に利用するdocker imageファイルの指定
など、コンテナを利用したサービスを提供する上で必要(そう)な機能を提供しています。

## アーキテクチャ
https://godaddy.github.io/2018/05/02/kubernetes-introduction-for-developers/

https://kubernetes.io/docs/concepts/architecture/

![アーキテクチャ](img/Chart_02_Kubernetes-Architecture.png)

![アーキテクチャ](img/Chart_04_Kubernetes-Node.png)

### Kubernetes Master
https://kubernetes.io/docs/concepts/overview/components/
- kubernetesの心臓部
  - k8sクラスタを管理するための機能を提供します。

#### kube-apiserver
k8s APIを処理するコンポーネント。

#### etcd
リソースの永続化に使われる高信頼分散KVS(key value store)
ノード間で共有する設定を保存している

### Image Registry
- k8sにdeployするコンテナイメージを格納する場所。
  - docker hubとか。
  - 個人的にはGitLab Container registryを使っています。

### クラスタ
- kubernetesを管理する1番大きなリソースの単位。ノードを束ねたもの。

### ノード
- k8sクラスタ1つに対し、1つ以上のノードで構成されます。
  - AKSにおいては、ノード=VM。ノードの上にコンテナが配備されます。

### ポッド
- ノード上で動作するアプリケーションのdeploy単位。
  - 1podに対し、1つ以上のコンテナが起動します。


### cloud shellを用いた操作

なんかGUIだとAzureだけの知識になってしまうので以降はCLIベースで。

今回はcloud shellを利用して行います。(必要なソフトウェアがある程度揃っているので)

なお、ボリューム料金が発生します。

自前でCLI環境が欲しい場合や、gitlab-ciに組み込んだりする場合は、以下のdocker-imageを使うとよいと思います。

https://hub.docker.com/r/microsoft/azure-cli/


### クラスタ接続準備

#### コンテキスト情報の確認

k8sクラスタへの接続にはコンテキスト情報を介して行います。
kubectlコマンドにてコンテキスト一覧を確認できます。

```
admin@Azure:~$ kubectl config get-contexts
CURRENT   NAME      CLUSTER   AUTHINFO   NAMESPACE
```

k8sクラスタ作成直後はコンテキスト情報がないので、結果は空です。

#### コンテキスト情報の作成

コンテキスト情報の作成はazコマンドを用いて行います。
cloud shellには既にinstallされているのでこちらを利用します。

- --resource-group
  - リソースグループ名
- --name
  - kubernetesサービス名

クレデンシャルの取得はGKE(Googleのk8sマネージド)も同じような操作です。

`e.g. gcloud ....`


```
admin@Azure:~$ az aks get-credentials --resource-group s-taira --name cl-k8s
Merged "cl-k8s" as current context in /home/admin/.kube/config

admin@Azure:~$ kubectl config get-contexts
CURRENT   NAME      CLUSTER   AUTHINFO                     NAMESPACE
*         cl-k8s    cl-k8s    clusterUser_s-taira_cl-k8s
admin@Azure:~$
```

こんな感じでコンテキストができました。
CURRENTに*がついているのがカレントコンテキスト。

### node情報

```
admin@Azure:~$ kubectl get node
NAME                       STATUS    ROLES     AGE       VERSION
aks-agentpool-81047232-0   Ready     agent     35m       v1.11.2
aks-agentpool-81047232-1   Ready     agent     34m       v1.11.2
aks-agentpool-81047232-2   Ready     agent     34m       v1.11.2
```

作成時に3ノードで指定したので見えてますね。

![作成画面](img/vms.png)
これらのVMがk8sクラスタのnodeとして構成されています。

### アプリケーションのdeploy

#### リソース情報の事前確認

```
admin@Azure:~$ kubectl get all
NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
service/kubernetes   ClusterIP   10.0.0.1     <none>        443/TCP   33m
```

最初は何もありません。

#### deploying

https://docs.microsoft.com/ja-jp/azure/aks/kubernetes-walkthrough-portal

ここの「アプリケーションの実行」をやります。

kubernetesへのdeployはyamlファイルを介して行われます。


##### deploy

```
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: azure-vote-back
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: azure-vote-back
    spec:
      containers:
      - name: azure-vote-back
        image: redis
        ports:
        - containerPort: 6379
          name: redis
---
apiVersion: v1
kind: Service
metadata:
  name: azure-vote-back
spec:
  ports:
  - port: 6379
  selector:
    app: azure-vote-back
---
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: azure-vote-front
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: azure-vote-front
    spec:
      containers:
      - name: azure-vote-front
        image: microsoft/azure-vote-front:v1
        ports:
        - containerPort: 80
        env:
        - name: REDIS
          value: "azure-vote-back"
---
apiVersion: v1
kind: Service
metadata:
  name: azure-vote-front
spec:
  type: LoadBalancer
  ports:
  - port: 80
  selector:
    app: azure-vote-front
```

上記をyamlファイルとして保存し、`kubectl apply -f xxxxx.yaml`を実行します。

```
admin@Azure:~$ kubectl get all
NAME                                    READY     STATUS    RESTARTS   AGE
pod/azure-vote-back-f9cc849fb-mmlc2     1/1       Running   0          6m
pod/azure-vote-front-5dfb9dd97b-wd82t   1/1       Running   0          6m

NAME                       TYPE           CLUSTER-IP    EXTERNAL-IP    PORT(S)        AGE
service/azure-vote-back    ClusterIP      10.0.76.146   <none>         6379/TCP       6m
service/azure-vote-front   LoadBalancer   10.0.247.74   13.78.38.151   80:32588/TCP   6m
service/kubernetes         ClusterIP      10.0.0.1      <none>         443/TCP        48m

NAME                               DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/azure-vote-back    1         1         1            1           6m
deployment.apps/azure-vote-front   1         1         1            1           6m

NAME                                          DESIRED   CURRENT   READY     AGE
replicaset.apps/azure-vote-back-f9cc849fb     1         1         1         6m
replicaset.apps/azure-vote-front-5dfb9dd97b   1         1         1         6m
```

上から順に、pod、service、deployment、replicasetです。

###### pod
- service経由でアクセスされます。
  - `kubectl portforward`コマンドを利用すれば、serviceがなくても、特定のホスト(localhost)からアクセスすることも可能です
- service->podでアクセスが割り振られています。
- 1podの中に複数コンテナが配置できます。(今回は1pod1コンテナです)
- 例えば1podの中に、Javaアプリケーションコンテナと、Javaアプリケーションを監視するJMXコンテナを1つのpodの中に入れるような形でpodを利用します。
- 1コンテナ1プロセスが原則です。

###### service
- podへ通信するための経路です。
- EXTERNAL-IP
  - 外部公開されているIPアドレスです。
  - http://13.78.38.151 にアクセスするとサンプルアプリが表示されます。
  - なお、Azureにおいては、`Public IPアドレス`が作成されてそれが割り当てられます。
- CLUSTER-IP
  - pod間の通信の際に使われるIPアドレス


###### replicaset
- podの状態管理をします。
- あらかじめ指定されたpod数と差分がある場合に自動的にpodを作成したり、削除したりします。
- つまりセルフヒーリング。


###### deployment
- replicasetを管理します。
- また、デプロイするものを定義します。
  - 利用するdocker imageの指定
  - podに割り当てるリソース(cpu/mem)の定義
  - persistent volume(永続化ボリューム)の定義
  - podをdeployするルール(pod affinity)
など、アプリケーション(image)以外に必要な構成を定義するためのものです。
なので、例えばpodのリソースを更新する場合は、deploymentを更新します。

https://kubernetes.io/docs/concepts/workloads/controllers/deployment/


### おまけ

#### その1
試しにpublic-ipにDNSの設定をします。

![public ip](img/public-ip.png)
この状態で http://cl-sample-lb.japaneast.cloudapp.azure.com/ にアクセスします。

![sample app](img/sample_app.png)
はい。表示されました。


#### その2
コンテナへのログイン
```
admin@Azure:~$ kubectl exec -it azure-vote-front-5dfb9dd97b-wd82t /bin/bash
root@azure-vote-front-5dfb9dd97b-wd82t:/app
```
コンテナが2つ以上存在する場合は、`-c <container-name>`でコンテナ名を指定します。

ログの確認(標準出力の内容を出力します)
```
admin@Azure:~$ kubectl logs azure-vote-front-5dfb9dd97b-wd82t
```

### まとめ
- k8sはpod単位でアプリケーションや製品をdeployする
- yamlファイルを利用してdeployする
- AKSを利用してk8sクラスタ、サービス、ボリュームなどを作成した場合は、裏でAzureのリソースが作成される
