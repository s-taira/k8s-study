# AKSでhelm

https://docs.helm.sh/helm/

https://docs.microsoft.com/ja-jp/azure/aks/kubernetes-helm

helmはpod/serviceなどのk8sリソース群をパッケージ管理するためのツールです。

ここを読んでいる時点ではyumやaptのk8s版だと思っていただければと思います。

## helm 実行環境の用意

今回はRBAC無効な環境で実施します。
Azureのcloud shellを利用して行います。

## 状態確認
helmはclient/server(k8s cluter)双方に導入する必要があります。
server側のhelmはtillerと呼ばれます。
※ver3からこの仕組みがごっそり変わるみたいですが、とりあえず今は2系で。

```
admin@Azure:~$ helm version
Client: &version.Version{SemVer:"v2.10.0", GitCommit:"9ad53aac42165a5fadc6c87be0dea6b115f93090", GitTreeState:"clean"}
Error: could not find tiller
admin@Azure:~$
```

cloud shellの環境ではhelmがinstallされているため、clientのバージョンが表示されます。
k8sクラスタ側にはまだinstallされていないためcould not find tillerが返ってきます。

## helm tillerのinstall
`helm init`でinstallできます。
--client only オプションをつけた場合はクライアントにのみinstallされます。
特にオプションを指定しない場合は、インストール先のk8sクラスタは、currentのconfigになります。

```
admin@Azure:~$ kubectl config current-context
cl-k8s
```

install先を変更する場合は、カレントコンテキストを変更しておくか、`--kube-context <str>`で指定可能です。

詳しくは`helm -h`で確認してもらえればと思います。

```
admin@Azure:~$ helm init
Creating /home/admin/.helm
Creating /home/admin/.helm/repository
Creating /home/admin/.helm/repository/cache
Creating /home/admin/.helm/repository/local
Creating /home/admin/.helm/plugins
Creating /home/admin/.helm/starters
Creating /home/admin/.helm/cache/archive
Creating /home/admin/.helm/repository/repositories.yaml
Adding stable repo with URL: https://kubernetes-charts.storage.googleapis.com
Adding local repo with URL: http://127.0.0.1:8879/charts
$HELM_HOME has been configured at /home/admin/.helm.

Tiller (the Helm server-side component) has been installed into your Kubernetes Cluster.

Please note: by default, Tiller is deployed with an insecure 'allow unauthenticated users' policy.
To prevent this, run `helm init` with the --tiller-tls-verify flag.
For more information on securing your installation see: https://docs.helm.sh/using_helm/#securing-your-helm-installation
Happy Helming!
```
Happy Helming!

```
admin@Azure:~$ helm version
Client: &version.Version{SemVer:"v2.10.0", GitCommit:"9ad53aac42165a5fadc6c87be0dea6b115f93090", GitTreeState:"clean"}
Server: &version.Version{SemVer:"v2.10.0", GitCommit:"9ad53aac42165a5fadc6c87be0dea6b115f93090", GitTreeState:"clean"}
```
これでhelmのセットアップが終わりました。

```
admin@Azure:~$ kubectl get pod -n kube-system | grep tiller
tiller-deploy-64c9d747bd-mgx75                                    1/1       Running   0          3m
admin@Azure:~$
```

tiller側もちゃんと入ってますね。

# repositoryの確認

```
admin@Azure:~$ helm repo list
NAME    URL
stable  https://kubernetes-charts.storage.googleapis.com
local   http://127.0.0.1:8879/charts
```
デフォルトでは上記2つのrepositoryが入っています。

stableは以下です。

https://github.com/helm/charts/tree/master/stable

あと有名どころでは`incubator`があります。

せっかくなので追加してみましょう。

# repositoryの追加

```
admin@Azure:~$ helm repo add incubator https://kubernetes-charts-incubator.storage.googleapis.com/
"incubator" has been added to your repositories
admin@Azure:~$ helm repo list
NAME            URL
stable          https://kubernetes-charts.storage.googleapis.com
local           http://127.0.0.1:8879/charts
incubator       https://kubernetes-charts-incubator.storage.googleapis.com/
```

# helmを使ったアプリケーションインストール

メッセージング基盤である[kafka](https://kafka.apache.org/)をhelmを使って導入します。

kafkaのhelm chartは[こちら](https://github.com/helm/charts/tree/master/incubator/kafka)です。


## パラメータ設定

製品を利用する際に用途に応じて調整したいパラメータがあると思います。

それらはhelmの思想としては、values.yamlというファイルに定義されており、利用者はそのパラメータを変更し、実際に製品をdeployします。

https://github.com/helm/charts/blob/master/incubator/kafka/values.yaml

いくつかみてみましょう。

### ポッド数
```
## The StatefulSet installs 3 pods by default
replicas: 3
```
いくつkafkaを作成するかの設定です。デフォルトでは3つのkafka用podが作られます。


### イメージプルポリシー
```
## Specify a imagePullPolicy
## ref: http://kubernetes.io/docs/user-guide/images/#pre-pulling-images
imagePullPolicy: "IfNotPresent"
```
- コンテナイメージをプルする際の設定です。
- "IfNotPresent"はキャッシュされたイメージを使うための設定です。
- ただし、コンテナのタグが"latest"の場合はこの値は無視されて常にimageをpullしにいきます。

ref: でk8s documentへのリンクがあるとおり、k8の機能です。

### CPU/MEMの割り当て設定
```
## Configure resource requests and limits
## ref: http://kubernetes.io/docs/user-guide/compute-resources/
resources: {}
  # limits:
  #   cpu: 200m
  #   memory: 1536Mi
  # requests:
  #   cpu: 100m
  #   memory: 1024Mi
kafkaHeapOptions: "-Xmx1G -Xms1G"
```
- resources
  - CPU、メモリの割り当て設定です。
  - 200mは0.2core(0.2vCPU)です。
  - 1000mは1core。"1"と記載しても同じ意味です。
  - requestsは初期割り当て、limitは最大割り当てです。
  - defaultでは省略されていますが、本番利用時は必ず指定するようにしましょう。
  - これはk8sのリソース管理機能です。
- kafkaHeapOptions
  - kafkaはJavaアプリなのでJVMのオプションになります。
  - kafkaプロセスを起動する際のオプションになります。
  - これはk8sのリソース管理機能ではないです。


### podの配置ルール
```
affinity: {}
## Alternatively, this typical example defines:
## antiAffinity (to keep Kafka pods on separate pods)
## and affinity (to encourage Kafka pods to be collocated with Zookeeper pods)
# affinity:
#   podAntiAffinity:
#     requiredDuringSchedulingIgnoredDuringExecution:
#     - labelSelector:
#         matchExpressions:
#         - key: app
#           operator: In
#           values:
#           - kafka
#       topologyKey: "kubernetes.io/hostname"
#   podAffinity:
#     preferredDringSchedulingIgnoredDuringExecution:
#      - weight: 50
#        podAffinityTerm:
#          labelSelector:
#            matchExpressions:
#            - key: app
#              operator: In
#              values:
#                - zookeeper
#          topologyKey: "kubernetes.io/hostname"
```

podをどのように配置するかのアフィニティ設定です。
- 既にkafka podが存在しているnodeには別のkafkaのpodを起動させない
  - 同じnode上にkafkaを乗せない、冗長性のための設定
- kafka podが起動しているnodeと同じnodeにzookeeperをのせる。

コメントアウトされている部分の意味は上記となります。

### JMX exporter取得設定
```
## Prometheus Exporters / Metrics
##
prometheus:
  ## Prometheus JMX Exporter: exposes the majority of Kafkas metrics
  jmx:
    enabled: false
```
[prometheus](https://prometheus.io/)のメトリクス情報を収集するかの設定
こちらをenabledにするとjmxで採取した情報をprometheusで見るためのexporterを作ります。

### kafka exporter取得設定
```
  ## Prometheus Kafka Exporter: exposes complimentary metrics to JMX Exporter
  kafka:
    enabled: false
```
kafka用のprometheusメトリクス情報を取得するかの設定。
broker数などのkafkaのメトリクス情報を採取します。


### exporterのprometheus連携設定
```
operator:
  ## Are you using Prometheus Operator?
  enabled: false

  serviceMonitor:
    # Namespace Prometheus is installed in
    namespace: monitoring

    ## Defaults to whats used if you follow CoreOS [Prometheus Install Instructions](https://github.com/coreos/prometheus-operator/tree/master/helm#tldr)
    ## [Prometheus Selector Label](https://github.com/coreos/prometheus-operator/blob/master/helm/prometheus/templates/prometheus.yaml#L65)
    ## [Kube Prometheus Selector Label](https://github.com/coreos/prometheus-operator/blob/master/helm/kube-prometheus/values.yaml#L298)
    selector:
      prometheus: kube-prometheus
```
coreOSが提供しているPrometheus Operatorを導入している場合、上記で設定したexporterの情報を
prometheusから見れるようにします。要はprometheusとの連携設定になります。

### その他たくさんありますが
割愛します。

- values.yamlは値を指定するためのファイルなので、実際の処理はtemplatesフォルダにあるyamlにて行われます。
- Helmの仕様としては、templatesフォルダ内にあるyamlは一律コールされます。

---------------------------------------

### helm 中間まとめ

といったようにさまざまな設定ができるようなものがhelmのchartです。

冒頭はわかりやすいように、「yumやaptのk8s版だと思っていただければ」と書きましたが、自分の中では

**「製品インストール＋パラメータ設定＋必要な製品との連携」まで見据えた利用できる状態で製品を設定するためのもの**

と理解しています。

もちろんtemplatesフォルダ内にそのような処理が書かれていなければ、設定されないので注意が必要です。

結構helm chartごとにばらつきがある印象です。


## installing kafka from helm!

### prometheus operator / prometheusのinstall

さきほど説明したprometheus連携したkafkaをinstallしたいと思います。
なので、まずはprometheus operatorからinstallします。
k8sの界隈は「kubernetes operators」というのがあって、まだまだ対象製品数は少ないですが、これから
「hogehoge operator」がどんどん増えていくのかなと思います。
prometheus operatorはprometheusの設定を反映したりしてくれるものです。

https://coreos.com/operators/
https://github.com/coreos/prometheus-operator/tree/master/helm/prometheus-operator

#### installing prometheus operator
```
admin@Azure:~$ helm repo add coreos https://s3-eu-west-1.amazonaws.com/coreos-charts/stable/
"coreos" has been added to your repositories
admin@Azure:~$ helm install coreos/prometheus-operator --name prometheus-operator --namespace monitoring --set rbacEnable=false,exporter-kube-state.rbacEnable=false,alertmanager.rbacEnable=false,prometheus.rbacEnable=false

NAME:   prometheus-operator
LAST DEPLOYED: Fri Sep 21 15:51:18 2018
NAMESPACE: monitoring
STATUS: DEPLOYED

RESOURCES:
==> v1beta1/PodSecurityPolicy
NAME                 DATA   CAPS      SELINUX   RUNASUSER  FSGROUP    SUPGROUP  READONLYROOTFS  VOLUMES
prometheus-operator  false  RunAsAny  RunAsAny  MustRunAs  MustRunAs  false     configMap,emptyDir,projected,secret,downwardAPI,persistentVolumeClaim

==> v1/ConfigMap
NAME                 DATA  AGE
prometheus-operator  1     2m

==> v1beta1/Deployment
NAME                 DESIRED  CURRENT  UP-TO-DATE  AVAILABLE  AGE
prometheus-operator  1        1        1           1          2m

==> v1/Pod(related)
NAME                                  READY  STATUS   RESTARTS  AGE
prometheus-operator-76c65c6d89-lml9f  1/1    Running  0         2m


NOTES:
The Prometheus Operator has been installed. Check its status by running:
  kubectl --namespace monitoring get pods -l "app=prometheus-operator,release=prometheus-operator"

Visit https://github.com/coreos/prometheus-operator for instructions on how
to create & configure Alertmanager and Prometheus instances using the Operator.

```
#### installing prometheus
```
helm install coreos/kube-prometheus --name kube-prometheus --set global.rbacEnable=false --namespace monitoring

admin@Azure:~$ helm install coreos/prometheus-operator --name prometheus-operator --namespace monitoring --set rbacEnable=false,exporter-kube-state.rbacEnable=false,alertmanager.rbacEnable=false,prometheus.rbacEnable=false

NAME:   prometheus-operator
LAST DEPLOYED: Fri Sep 21 15:51:18 2018
NAMESPACE: monitoring
STATUS: DEPLOYED

RESOURCES:
==> v1beta1/PodSecurityPolicy
NAME                 DATA   CAPS      SELINUX   RUNASUSER  FSGROUP    SUPGROUP  READONLYROOTFS  VOLUMES
prometheus-operator  false  RunAsAny  RunAsAny  MustRunAs  MustRunAs  false     configMap,emptyDir,projected,secret,downwardAPI,persistentVolumeClaim

==> v1/ConfigMap
NAME                 DATA  AGE
prometheus-operator  1     2m

==> v1beta1/Deployment
NAME                 DESIRED  CURRENT  UP-TO-DATE  AVAILABLE  AGE
prometheus-operator  1        1        1           1          2m

==> v1/Pod(related)
NAME                                  READY  STATUS   RESTARTS  AGE
prometheus-operator-76c65c6d89-lml9f  1/1    Running  0         2m


NOTES:
The Prometheus Operator has been installed. Check its status by running:
  kubectl --namespace monitoring get pods -l "app=prometheus-operator,release=prometheus-operator"

Visit https://github.com/coreos/prometheus-operator for instructions on how
to create & configure Alertmanager and Prometheus instances using the Operator.

admin@Azure:~$
admin@Azure:~$ helm install coreos/kube-prometheus --name kube-prometheus --set global.rbacEnable=false --namespace monitoring
NAME:   kube-prometheus
LAST DEPLOYED: Fri Sep 21 15:54:38 2018
NAMESPACE: monitoring
STATUS: DEPLOYED

RESOURCES:
==> v1/PrometheusRule
NAME                                              AGE
kube-prometheus-alertmanager                      5s
kube-prometheus-exporter-kube-controller-manager  5s
kube-prometheus-exporter-kube-etcd                5s
kube-prometheus-exporter-kube-scheduler           5s
kube-prometheus-exporter-kube-state               4s
kube-prometheus-exporter-kubelets                 4s
kube-prometheus-exporter-kubernetes               4s
kube-prometheus-exporter-node                     4s
kube-prometheus-rules                             4s
kube-prometheus                                   4s

==> v1/ServiceMonitor
kube-prometheus-alertmanager                      4s
kube-prometheus-exporter-kube-controller-manager  4s
kube-prometheus-exporter-kube-dns                 4s
kube-prometheus-exporter-kube-etcd                4s
kube-prometheus-exporter-kube-scheduler           4s
kube-prometheus-exporter-kube-state               4s
kube-prometheus-exporter-kubelets                 4s
kube-prometheus-exporter-kubernetes               4s
kube-prometheus-exporter-node                     4s
kube-prometheus-grafana                           4s
kube-prometheus                                   4s

==> v1beta1/PodSecurityPolicy
NAME                                 DATA   CAPS      SELINUX   RUNASUSER  FSGROUP    SUPGROUP  READONLYROOTFS  VOLUMES
kube-prometheus-alertmanager         false  RunAsAny  RunAsAny  MustRunAs  MustRunAs  false     configMap,emptyDir,projected,secret,downwardAPI,persistentVolumeClaim
kube-prometheus-exporter-kube-state  false  RunAsAny  RunAsAny  MustRunAs  MustRunAs  false     configMap,emptyDir,projected,secret,downwardAPI,persistentVolumeClaim
kube-prometheus-exporter-node        false  RunAsAny  RunAsAny  MustRunAs  MustRunAs  false     configMap,emptyDir,projected,secret,downwardAPI,persistentVolumeClaim,hostPath
kube-prometheus-grafana              false  RunAsAny  RunAsAny  MustRunAs  MustRunAs  false     configMap,emptyDir,projected,secret,downwardAPI,persistentVolumeClaim,hostPath
kube-prometheus                      false  RunAsAny  RunAsAny  MustRunAs  MustRunAs  false     configMap,emptyDir,projected,secret,downwardAPI,persistentVolumeClaim

==> v1beta1/Deployment
NAME                                 DESIRED  CURRENT  UP-TO-DATE  AVAILABLE  AGE
kube-prometheus-exporter-kube-state  1        1        1           0          5s
kube-prometheus-grafana              1        1        1           0          5s

==> v1/Alertmanager
NAME             AGE
kube-prometheus  5s

==> v1beta1/DaemonSet
NAME                           DESIRED  CURRENT  READY  UP-TO-DATE  AVAILABLE  NODE SELECTOR  AGE
kube-prometheus-exporter-node  3        3        0      3           0          <none>         5s

==> v1/Prometheus
NAME             AGE
kube-prometheus  5s

==> v1/Pod(related)
NAME                                                READY  STATUS             RESTARTS  AGE
kube-prometheus-exporter-node-b676m                 0/1    ContainerCreating  0         5s
kube-prometheus-exporter-node-m5vvb                 0/1    ContainerCreating  0         5s
kube-prometheus-exporter-node-mk9s9                 0/1    ContainerCreating  0         5s
kube-prometheus-exporter-kube-state-78bf77db-ndlmc  0/2    ContainerCreating  0         5s
kube-prometheus-grafana-6fb79c6c58-85jlk            0/2    ContainerCreating  0         5s

==> v1/Secret
NAME                          TYPE    DATA  AGE
alertmanager-kube-prometheus  Opaque  1     5s
kube-prometheus-grafana       Opaque  2     5s

==> v1/ConfigMap
NAME                     DATA  AGE
kube-prometheus-grafana  10    5s

==> v1/Service
NAME                                              TYPE       CLUSTER-IP    EXTERNAL-IP  PORT(S)              AGE
kube-prometheus-alertmanager                      ClusterIP  10.0.58.119   <none>       9093/TCP             5s
kube-prometheus-exporter-kube-controller-manager  ClusterIP  None          <none>       10252/TCP            5s
kube-prometheus-exporter-kube-dns                 ClusterIP  None          <none>       10054/TCP,10055/TCP  5s
kube-prometheus-exporter-kube-etcd                ClusterIP  None          <none>       4001/TCP             5s
kube-prometheus-exporter-kube-scheduler           ClusterIP  None          <none>       10251/TCP            5s
kube-prometheus-exporter-kube-state               ClusterIP  10.0.231.141  <none>       80/TCP               5s
kube-prometheus-exporter-node                     ClusterIP  10.0.17.227   <none>       9100/TCP             5s
kube-prometheus-grafana                           ClusterIP  10.0.171.74   <none>       80/TCP               5s
kube-prometheus                                   ClusterIP  10.0.102.179  <none>       9090/TCP             5s


NOTES:
DEPRECATION NOTICE:

- alertmanager.ingress.fqdn is not used anymore, use alertmanager.ingress.hosts []
- prometheus.ingress.fqdn is not used anymore, use prometheus.ingress.hosts []
- grafana.ingress.fqdn is not used anymore, use prometheus.grafana.hosts []

- additionalRulesConfigMapLabels is not used anymore, use additionalRulesLabels
- prometheus.additionalRulesConfigMapLabels is not used anymore, use additionalRulesLabels
- alertmanager.additionalRulesConfigMapLabels is not used anymore, use additionalRulesLabels
- exporter-kube-controller-manager.additionalRulesConfigMapLabels is not used anymore, use additionalRulesLabels
- exporter-kube-etcd.additionalRulesConfigMapLabels is not used anymore, use additionalRulesLabels
- exporter-kube-scheduler.additionalRulesConfigMapLabels is not used anymore, use additionalRulesLabels
- exporter-kubelets.additionalRulesConfigMapLabels is not used anymore, use additionalRulesLabels
- exporter-kubernetes.additionalRulesConfigMapLabels is not used anymore, use additionalRulesLabels

```

#### 状態確認
monitoring namespaceにinstallしたため、「-n monitoring」をつけて確認します。

```
admin@Azure:~$ kubectl get all -n monitoring
NAME                                                       READY     STATUS    RESTARTS   AGE
pod/alertmanager-kube-prometheus-0                         2/2       Running   0          1m
pod/kube-prometheus-exporter-kube-state-7d964949f4-94bdh   2/2       Running   0          1m
pod/kube-prometheus-exporter-node-b676m                    1/1       Running   0          1m
pod/kube-prometheus-exporter-node-m5vvb                    1/1       Running   0          1m
pod/kube-prometheus-exporter-node-mk9s9                    1/1       Running   0          1m
pod/kube-prometheus-grafana-6fb79c6c58-85jlk               2/2       Running   0          1m
pod/prometheus-kube-prometheus-0                           3/3       Running   1          1m
pod/prometheus-operator-76c65c6d89-lml9f                   1/1       Running   0          5m

NAME                                          TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)             AGE
service/alertmanager-operated                 ClusterIP   None           <none>        9093/TCP,6783/TCP   1m
service/kube-prometheus                       ClusterIP   10.0.102.179   <none>        9090/TCP            1m
service/kube-prometheus-alertmanager          ClusterIP   10.0.58.119    <none>        9093/TCP            1m
service/kube-prometheus-exporter-kube-state   ClusterIP   10.0.231.141   <none>        80/TCP              1m
service/kube-prometheus-exporter-node         ClusterIP   10.0.17.227    <none>        9100/TCP            1m
service/kube-prometheus-grafana               ClusterIP   10.0.171.74    <none>        80/TCP              1m
service/prometheus-operated                   ClusterIP   None           <none>        9090/TCP            1m

NAME                                           DESIRED   CURRENT   READY     UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
daemonset.apps/kube-prometheus-exporter-node   3         3         3         3            3           <none>          1m

NAME                                                  DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/kube-prometheus-exporter-kube-state   1         1         1            1           1m
deployment.apps/kube-prometheus-grafana               1         1         1            1           1m
deployment.apps/prometheus-operator                   1         1         1            1           5m

NAME                                                             DESIRED   CURRENT   READY     AGE
replicaset.apps/kube-prometheus-exporter-kube-state-78bf77db     0         0         0         1m
replicaset.apps/kube-prometheus-exporter-kube-state-7d964949f4   1         1         1         1m
replicaset.apps/kube-prometheus-grafana-6fb79c6c58               1         1         1         1m
replicaset.apps/prometheus-operator-76c65c6d89                   1         1         1         5m

NAME                                            DESIRED   CURRENT   AGE
statefulset.apps/alertmanager-kube-prometheus   1         1         1m
statefulset.apps/prometheus-kube-prometheus     1         1         1m
```

なんかいっぱい入ってますね。
各ノードの情報を収集するnode exporterが入ったり、ダッシュボードとしてgrafanaが入ったりしています。
これもkafkaのときと同じようにいろんな設定が入った状態で製品が導入されます。

#### prometheusにアクセスしてみる

```
admin@Azure:~$ kubectl -n monitoring port-forward prometheus-kube-prometheus-0 9090:9090
Forwarding from 127.0.0.1:9090 -> 9090
Forwarding from [::1]:9090 -> 9090
```

これでcloud shell(≒k8sクライアント)へprometheusのpodを転送。
…ここからどうやってlocalhostまで転送するんだろう・・・。
通常はlocalhostまで転送し、ブラウザで"localhost:xxxxx"にアクセスするとprometheusのUIが表示されます。

実際にブラウザで目に見える形で動いていることを確認するために、実際の環境ではまずやらない（やってはいけない）ですが、prometheusのUIに外部からアクセスする口を作ります。

```
apiVersion: v1
kind: Service
metadata:
  name: service-sample
spec:
  type: LoadBalancer
  ports:
  - port: 9090
  selector:
    app: prometheus
```
上記のyamlファイルを作成し、sample-service.yamlとして保存します。
`kubectl apply -f sample-service.yaml -n monitoring`を実行します。

簡単に内容を説明すると、app=prometheusのがラベルがついているpodにリクエストを転送するための設定です。

今回installしたprometheusには、key:app value：prometheusのラベルがついています。

そのあと、`kubectL get svc -n monitoring`でservice-sampleのEXTERNAL IPが払い出されるのを待ち、払い出されたらブラウザからアクセスします。

http://xxxxxxxxx:9090

そうするとprometheusのUIが表示されます。

試しにtargetにアクセスしてみましょう。k8s関連のリソースを収集する仕掛けが入っていますね。

**しつこいですが、この手順で外部から穴をあけてはいけません**

### installing kafka

本題のkafka installです。

```
admin@Azure:~$ helm install -n happykafka incubator/kafka --set prometheus.jmx.enabled=true,prometheus.kafka.enabled=true,prometheus.operator.enabled=true --namespace kafka
NAME:   happykafka
LAST DEPLOYED: Fri Sep 21 16:21:20 2018
NAMESPACE: kafka
STATUS: DEPLOYED
```

#### helm release確認
```
admin@Azure:~$ helm list
NAME                    REVISION        UPDATED                         STATUS          CHART                           APP VERSION     NAMESPACE
happykafka              1               Fri Sep 21 16:21:20 2018        DEPLOYED        kafka-0.10.0                    4.1.2           kafka
kube-prometheus         1               Fri Sep 21 15:54:38 2018        DEPLOYED        kube-prometheus-0.0.105                         monitoring
prometheus-operator     1               Fri Sep 21 15:51:18 2018        DEPLOYED        prometheus-operator-0.0.29      0.20.0          monitoring
```

helmでdeployしたリソースはリビジョン管理されます。
update、rollback、deleteはhelmコマンドで行いそちらで管理します。

#### kafka pod起動確認
```
admin@Azure:~$ kubectl get pod -n kafka
NAME                                   READY     STATUS    RESTARTS   AGE
happykafka-0                           2/2       Running   0          3m
happykafka-1                           2/2       Running   0          5m
happykafka-2                           2/2       Running   0          6m
happykafka-exporter-7666fc6c59-jsd8c   1/1       Running   1          3m
happykafka-zookeeper-0                 1/1       Running   0          3m
happykafka-zookeeper-1                 1/1       Running   0          2m
happykafka-zookeeper-2                 1/1       Running   0          1m
```
- prometheus.jmx.enabled=true
    - kafka-0内にjmx用のコンテナが起動しています(2/2)
- prometheus.kafka.enabled=true
    - kafka用のexporterが表示されます。
- prometheus.operator.enabled=true
    - kafkaとprometheusが連携された状態になります。


### prometheus画面で監視対象を確認する

![prometheus-kafka連携画面](img/prometheus-kafka.png)
このようにkafkaのjmx情報およびkafkaのexporter情報を取得する設定がprometheusに追加されます。

なお、prometheusのmetrics情報は以下のような感じです。

```
root@happykafka-0:/# curl <kafka-exporter pod IP>:9308/metrics
# HELP go_gc_duration_seconds A summary of the GC invocation durations.
# TYPE go_gc_duration_seconds summary
go_gc_duration_seconds{quantile="0"} 2.7201e-05
go_gc_duration_seconds{quantile="0.25"} 9.0002e-05
go_gc_duration_seconds{quantile="0.5"} 0.000176002
go_gc_duration_seconds{quantile="0.75"} 0.000208303
go_gc_duration_seconds{quantile="1"} 0.000944512
go_gc_duration_seconds_sum 0.006500583
go_gc_duration_seconds_count 33
# HELP go_goroutines Number of goroutines that currently exist.
# TYPE go_goroutines gauge
go_goroutines 11
# HELP go_memstats_alloc_bytes Number of bytes allocated and still in use.
# TYPE go_memstats_alloc_bytes gauge
go_memstats_alloc_bytes 3.362368e+06
# HELP go_memstats_alloc_bytes_total Total number of bytes allocated, even if freed.
# TYPE go_memstats_alloc_bytes_total counter
go_memstats_alloc_bytes_total 9.8264008e+07
# HELP go_memstats_buck_hash_sys_bytes Number of bytes used by the profiling bucket hash table.
# TYPE go_memstats_buck_hash_sys_bytes gauge
go_memstats_buck_hash_sys_bytes 1.448556e+06
# HELP go_memstats_frees_total Total number of frees.
# TYPE go_memstats_frees_total counter
go_memstats_frees_total 177468
# HELP go_memstats_gc_sys_bytes Number of bytes used for garbage collection system metadata.
# TYPE go_memstats_gc_sys_bytes gauge
go_memstats_gc_sys_bytes 405504
# HELP go_memstats_heap_alloc_bytes Number of heap bytes allocated and still in use.
# TYPE go_memstats_heap_alloc_bytes gauge
go_memstats_heap_alloc_bytes 3.362368e+06
# HELP go_memstats_heap_idle_bytes Number of heap bytes waiting to be used.
# TYPE go_memstats_heap_idle_bytes gauge
go_memstats_heap_idle_bytes 1.794048e+06
# HELP go_memstats_heap_inuse_bytes Number of heap bytes that are in use.
# TYPE go_memstats_heap_inuse_bytes gauge
go_memstats_heap_inuse_bytes 4.038656e+06
# HELP go_memstats_heap_objects Number of allocated objects.
# TYPE go_memstats_heap_objects gauge
go_memstats_heap_objects 9312
# HELP go_memstats_heap_released_bytes_total Total number of heap bytes released to OS.
# TYPE go_memstats_heap_released_bytes_total counter
go_memstats_heap_released_bytes_total 0
# HELP go_memstats_heap_sys_bytes Number of heap bytes obtained from system.
# TYPE go_memstats_heap_sys_bytes gauge
go_memstats_heap_sys_bytes 5.832704e+06
# HELP go_memstats_last_gc_time_seconds Number of seconds since 1970 of last garbage collection.
# TYPE go_memstats_last_gc_time_seconds gauge
go_memstats_last_gc_time_seconds 1.5390671881710353e+09
# HELP go_memstats_lookups_total Total number of pointer lookups.
# TYPE go_memstats_lookups_total counter
go_memstats_lookups_total 1393
# HELP go_memstats_mallocs_total Total number of mallocs.
# TYPE go_memstats_mallocs_total counter
go_memstats_mallocs_total 186780
# HELP go_memstats_mcache_inuse_bytes Number of bytes in use by mcache structures.
# TYPE go_memstats_mcache_inuse_bytes gauge
go_memstats_mcache_inuse_bytes 3472
# HELP go_memstats_mcache_sys_bytes Number of bytes used for mcache structures obtained from system.
# TYPE go_memstats_mcache_sys_bytes gauge
go_memstats_mcache_sys_bytes 16384
# HELP go_memstats_mspan_inuse_bytes Number of bytes in use by mspan structures.
# TYPE go_memstats_mspan_inuse_bytes gauge
go_memstats_mspan_inuse_bytes 28576
# HELP go_memstats_mspan_sys_bytes Number of bytes used for mspan structures obtained from system.
# TYPE go_memstats_mspan_sys_bytes gauge
go_memstats_mspan_sys_bytes 49152
# HELP go_memstats_next_gc_bytes Number of heap bytes when next garbage collection will take place.
# TYPE go_memstats_next_gc_bytes gauge
go_memstats_next_gc_bytes 4.194304e+06
# HELP go_memstats_other_sys_bytes Number of bytes used for other system allocations.
# TYPE go_memstats_other_sys_bytes gauge
go_memstats_other_sys_bytes 773772
# HELP go_memstats_stack_inuse_bytes Number of bytes in use by the stack allocator.
# TYPE go_memstats_stack_inuse_bytes gauge
go_memstats_stack_inuse_bytes 458752
# HELP go_memstats_stack_sys_bytes Number of bytes obtained from system for stack allocator.
# TYPE go_memstats_stack_sys_bytes gauge
go_memstats_stack_sys_bytes 458752
# HELP go_memstats_sys_bytes Number of bytes obtained by system. Sum of all system allocations.
# TYPE go_memstats_sys_bytes gauge
go_memstats_sys_bytes 8.984824e+06
# HELP kafka_brokers Number of Brokers in the Kafka Cluster.
# TYPE kafka_brokers gauge
kafka_brokers 3
# HELP kafka_exporter_build_info A metric with a constant '1' value labeled by version, revision, branch, and goversion from which kafka_exporter was built.
# TYPE kafka_exporter_build_info gauge
kafka_exporter_build_info{branch="HEAD",goversion="go1.9",revision="830660212e6c109e69dcb1cb58f5159fe3b38903",version="1.2.0"} 1
# HELP kafka_topic_partition_current_offset Current Offset of a Broker at Topic/Partition
# TYPE kafka_topic_partition_current_offset gauge
kafka_topic_partition_current_offset{partition="0",topic="__confluent.support.metrics"} 3
# HELP kafka_topic_partition_in_sync_replica Number of In-Sync Replicas for this Topic/Partition
# TYPE kafka_topic_partition_in_sync_replica gauge
kafka_topic_partition_in_sync_replica{partition="0",topic="__confluent.support.metrics"} 1
# HELP kafka_topic_partition_leader Leader Broker ID of this Topic/Partition
# TYPE kafka_topic_partition_leader gauge
kafka_topic_partition_leader{partition="0",topic="__confluent.support.metrics"} 0
# HELP kafka_topic_partition_leader_is_preferred 1 if Topic/Partition is using the Preferred Broker
# TYPE kafka_topic_partition_leader_is_preferred gauge
kafka_topic_partition_leader_is_preferred{partition="0",topic="__confluent.support.metrics"} 1
# HELP kafka_topic_partition_oldest_offset Oldest Offset of a Broker at Topic/Partition
# TYPE kafka_topic_partition_oldest_offset gauge
kafka_topic_partition_oldest_offset{partition="0",topic="__confluent.support.metrics"} 0
# HELP kafka_topic_partition_replicas Number of Replicas for this Topic/Partition
# TYPE kafka_topic_partition_replicas gauge
kafka_topic_partition_replicas{partition="0",topic="__confluent.support.metrics"} 1
# HELP kafka_topic_partition_under_replicated_partition 1 if Topic/Partition is under Replicated
# TYPE kafka_topic_partition_under_replicated_partition gauge
kafka_topic_partition_under_replicated_partition{partition="0",topic="__confluent.support.metrics"} 0
# HELP kafka_topic_partitions Number of partitions for this Topic
# TYPE kafka_topic_partitions gauge
kafka_topic_partitions{topic="__confluent.support.metrics"} 1
# HELP process_cpu_seconds_total Total user and system CPU time spent in seconds.
# TYPE process_cpu_seconds_total counter
process_cpu_seconds_total 0.45
# HELP process_max_fds Maximum number of open file descriptors.
# TYPE process_max_fds gauge
process_max_fds 1.048576e+06
# HELP process_open_fds Number of open file descriptors.
# TYPE process_open_fds gauge
process_open_fds 9
# HELP process_resident_memory_bytes Resident memory size in bytes.
# TYPE process_resident_memory_bytes gauge
process_resident_memory_bytes 1.3197312e+07
# HELP process_start_time_seconds Start time of the process since unix epoch in seconds.
# TYPE process_start_time_seconds gauge
process_start_time_seconds 1.53906610077e+09
# HELP process_virtual_memory_bytes Virtual memory size in bytes.
# TYPE process_virtual_memory_bytes gauge
process_virtual_memory_bytes 1.744896e+07
```


### prometheusとgrafana

prometheusにもグラフ描写機能がありますが、grafanaだともっとそれっぽく綺麗に見えます。
一般的にはmetrics収集はprometheus、可視化はgrafana(データソースにprometheusを指定)とするケースが多いようです。
今回grafanaについては割愛します。

ちなみにgrafanaでkafka関連の情報を表示したらこんな感じです。

![kafka のmetrics情報をgrafanaで表示](img/kafka-grafana.png)

### クリーンアップ

#### service-sampleの削除
```
admin@Azure:~$ kubectl delete -f prometheus-external.yaml  -n monitoring
service "service-sample" deleted
admin@Azure:~$ kubectl get svc -n monitoring
NAME                                  TYPE     
CLUSTER-IP     EXTERNAL-IP   PORT(S)             AGE
alertmanager-operated                 ClusterIP   None           <none>        9093/TCP,6783/TCP   4h
kube-prometheus                       ClusterIP   10.0.17.232    <none>        9090/TCP            4h
kube-prometheus-alertmanager          ClusterIP   10.0.231.3     <none>        9093/TCP            4h
kube-prometheus-exporter-kube-state   ClusterIP   10.0.23.107    <none>        80/TCP              4h
kube-prometheus-exporter-node         ClusterIP   10.0.210.251   <none>        9100/TCP            4h
kube-prometheus-grafana               ClusterIP   10.0.13.4      <none>        80/TCP              4h
prometheus-operated                   ClusterIP   None           <none>        9090/TCP            4h
```

#### アプリケーションの削除
```
helm delete --purge happykafka
```
のように書くとhelmでdeployしたkafka APPが消えます。

`--purge`は完全消去です。

#### 永続化ボリューム(persistent volume)の削除

helm deleteを行っても永続化ボリュームは残ったままです。

`kubectl get pvc -n kafka`でpersistent volume chainを確認し、`kubectl delete pvc <PVC名> -n kafka`で削除します。

ちなみにprometheusやgrafanaはデフォルトではpersistent volumeを使う設定になっていないので、例えばgrafanaが落ちてしまうと後から追加したダッシュボードの設定が飛んでしまいます。

#### Azure GUIからの削除

Azure GUIからk8s関連のリソース削除をすると綺麗にすべて消してくれるみたいです。

メトリクスやログ監視などのサービス機能も消えるようなので、綺麗さっぱり消したいときはそちらから消すとよいと思います。

### helmまとめ

- プロダクトを利用可能な状態に設定するもの
- 使いこなすにはk8sと該当する製品の知識が必要（つらい）
- yamlファイルの集合体
- リソースの導入以外にも更新等にもhelmコマンドを使うことで構成管理される
- (触れてなかったけど)namespaceは名前空間でリソース群を箱に入れて管理するためのもの。RBACとか使えば権限管理にも使える。


















